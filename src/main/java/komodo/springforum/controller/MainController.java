package komodo.springforum.controller;

import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.User;
import komodo.springforum.service.entity.ChatRoomService;
import komodo.springforum.service.entity.MessageService;
import komodo.springforum.service.entity.UserRoleService;
import komodo.springforum.service.entity.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;
import java.util.Set;

/**
 * Created by i.kotelnikov on 04.09.2017.
 */
@Controller
@RequestMapping("/")
public class MainController {

    @Autowired
    private ChatRoomService chatRoomService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getIndex(Principal principal, Model model){
        model.addAttribute("title", "My Chat!");
        return "index";
    }

    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public String statisticChat(Model model){
        List<ChatRoom> chatRooms = chatRoomService.getAllChatRooms();
        model.addAttribute("chatRooms", chatRooms);
        return "stats";
    }

}
