package komodo.springforum.controller;

import komodo.springforum.service.websocket.OnlineUsersService;
import komodo.springforum.service.websocket.WebSocketMessageHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.PongMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

/**
 * Created by i.kotelnikov on 21.09.2017.
 */
@Component
public class ChatWsHandler extends TextWebSocketHandler {

    @Autowired
    OnlineUsersService onlineUsersService;

    @Autowired
    WebSocketMessageHandlerService messageHandlerService;

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws InterruptedException, IOException {
        messageHandlerService.handle(session, message);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        onlineUsersService.handleNewSession(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        onlineUsersService.handleSessionClosed(session, status);
    }

    @Override
    protected void handlePongMessage(WebSocketSession session, PongMessage message) throws Exception {
        //...
    }




}
