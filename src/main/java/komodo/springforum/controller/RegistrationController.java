package komodo.springforum.controller;

import komodo.springforum.controller.validator.RegisterValidator;
import komodo.springforum.model.form.RegistrationForm;
import komodo.springforum.service.entity.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by i.kotelnikov on 04.10.2017.
 */

@Controller
@RequestMapping("/init")
public class RegistrationController {

    @Autowired
    private RegisterValidator registerValidator;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String register(ModelMap model) {
        RegistrationForm registerForm = new RegistrationForm();
        model.put("registrationForm", registerForm);
        return "registration";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processRegister(RegistrationForm registerForm, BindingResult result, ModelMap model) {
        registerValidator.validate(registerForm, result);
        if (result.hasErrors()) {
            model.put("registrationForm", registerForm);
            return "registration";
        }
        userService.registerNewUser(registerForm.getUsername(), registerForm.getPassword());
        return "registration-success";
    }

}
