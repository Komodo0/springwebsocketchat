package komodo.springforum.controller.validator;

import komodo.springforum.model.form.RegistrationForm;
import komodo.springforum.service.entity.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by i.kotelnikov on 04.10.2017.
 */
@Component
public class RegisterValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return RegistrationForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationForm registerForm = (RegistrationForm) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "username.empty", "Username must not be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password.empty", "Password must not be empty.");

        String username = registerForm.getUsername();
        if ((username.length()) > 16) {
            errors.rejectValue("username", "username.tooLong", "Username must not more than 16 characters.");
        }
        if ((username.length()) < 3) {
            errors.rejectValue("username", "username.tooShort", "Username must not less than 3 characters.");
        }

        if (!(registerForm.getPassword()).equals(registerForm
                .getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "confirmPassword.passwordDontMatch", "Passwords don't match.");
        }

        if (userService.userExists(registerForm.getUsername())){
            errors.rejectValue("username", "username.alreadyExists", "This username already exists!");
        }

    }
}
