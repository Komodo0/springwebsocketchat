package komodo.springforum.controller;

import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.protocol.rest.ChatRoomStatistic;
import komodo.springforum.service.entity.ChatRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by i.kotelnikov on 05.10.2017.
 */
@Controller()
@RequestMapping("/rest")
public class RESTController {

    @Autowired
    ChatRoomService chatRoomService;

    @ResponseBody
    @RequestMapping(value = "/stats")
    public ChatRoomStatistic chatRoomStatistic(){
        List<ChatRoom> chatRooms = chatRoomService.getAllChatRooms();
        return new ChatRoomStatistic(chatRooms);
    }


}
