package komodo.springforum.model.enity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by i.kotelnikov on 11.09.2017.
 */
@Entity
@Table(name = "user_role", schema = "spring_app", uniqueConstraints = @UniqueConstraint(columnNames = { "role" }))
public class UserRole {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "role_id", unique = true, nullable = false)
    private int id;

    @Basic
    @Column(name = "role", nullable = false, length = 45)
    private String role;

//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinTable(name = "user_to_user_role",
//            joinColumns = @JoinColumn(name = "role_id", nullable = false, updatable = false),
//            inverseJoinColumns = @JoinColumn(name = "user_id", nullable = false, updatable = false))
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "userRoles", cascade = CascadeType.ALL)
    private Set<User> users = new HashSet<User>(0);

    public UserRole(){}

    public UserRole(String roleName){
        setRole(roleName);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

}
