package komodo.springforum.model.enity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by i.kotelnikov on 07.09.2017.
 */
@Entity
@Table(name = "message", schema = "spring_app")
public class Message {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Basic
    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Basic
    @Column(name = "content")
    private String content;

    @ManyToOne()
    @JoinColumn(name = "author")
    private User author;

    @ManyToOne()
    @JoinColumn(name = "chat_room")
    private ChatRoom chatRoom;

    @PrePersist
    protected void onCreate() {
        this.timestamp = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }
}
