package komodo.springforum.model.enity;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.WebSocketSession;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by i.kotelnikov on 12.09.2017.
 */
@Entity
@Table(name = "chat_room", schema = "spring_app", uniqueConstraints = @UniqueConstraint(columnNames = { "name" }))
public class ChatRoom {


    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Basic
    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "chatRoom")
    private Set<Message> messages = new HashSet<Message>(0);

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> loggedUsersHistory = new HashSet<>();

    @Transient
    private Set<User> users = new HashSet<>();

    public ChatRoom(){}

    public ChatRoom(String name){
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public boolean equals(ChatRoom chatRoom){
        return this.getName().equals(chatRoom.getName());
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<User> getLoggedUsersHistory() {
        return loggedUsersHistory;
    }

    public void setLoggedUsersHistory(Set<User> loggedUsersHistory) {
        this.loggedUsersHistory = loggedUsersHistory;
    }

    public void addLoggedUserToHistory(User user){
        for (User user1 : loggedUsersHistory){
            if (user1.getUsername().equals(user.getUsername())){
                return;
            }
        }
        this.loggedUsersHistory.add(user);
    }

    @Transactional
    public void addUser(User user){
        this.users.add(user);
    }

    @Transactional
    public void removeUser(String username){
        for (User user : this.users) {
            if (user.getUsername().equals(username)) {
                users.remove(user);
                break;
            }
        }
    }

    public boolean isEmpty(){
        return this.users.size() == 0;
    }
}
