package komodo.springforum.model.enity;

import org.springframework.web.socket.WebSocketSession;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by i.kotelnikov on 07.09.2017.
 */
@Entity
@Table(name = "user", schema = "spring_app", uniqueConstraints = @UniqueConstraint(columnNames = { "username" }))
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Basic
    @Column(name = "username")
    private String username;

    @Basic
    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_to_user_role",
            joinColumns = @JoinColumn(name = "user_id", nullable = false, updatable = false),
            inverseJoinColumns = @JoinColumn(name = "role_id", nullable = false, updatable = false))
    private Set<UserRole> userRoles = new HashSet<UserRole>(0);

    @Transient
    private WebSocketSession webSocketSession;

    @Transient
    private ChatRoom chatRoom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public WebSocketSession getWebSocketSession() {
        return webSocketSession;
    }

    public void setWebSocketSession(WebSocketSession webSocketSession) {
        this.webSocketSession = webSocketSession;
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    public boolean inChatRoom(){
        return this.chatRoom != null;
    }

    public void addUserRole(UserRole userRole){
        if (this.userRoles == null){
            this.userRoles = new HashSet<>();
        }
        this.userRoles.add(userRole);
    }

    public boolean equals(User anotherUser){
        return this.getId() == anotherUser.getId();
    }

}
