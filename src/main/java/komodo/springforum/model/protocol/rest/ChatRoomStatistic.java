package komodo.springforum.model.protocol.rest;

import komodo.springforum.model.enity.ChatRoom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by i.kotelnikov on 05.10.2017.
 */
public class ChatRoomStatistic {

    private Map<String ,Map<String, Integer>> statistic;

    public ChatRoomStatistic(List<ChatRoom> chatRooms){
        this.statistic = new HashMap<>();
        for (ChatRoom chatRoom : chatRooms){
            Map<String, Integer> chatRoomDetails = new HashMap<>();
            chatRoomDetails.put("UsersCount", chatRoom.getLoggedUsersHistory().size());
            chatRoomDetails.put("MessagesCount", chatRoom.getMessages().size());
            statistic.put(chatRoom.getName(), chatRoomDetails);
        }
    }

    public Map<String, Map<String, Integer>> getStatistic() {
        return statistic;
    }

    public void setStatistic(Map<String, Map<String, Integer>> statistic) {
        this.statistic = statistic;
    }
}
