package komodo.springforum.model.protocol.output;

import komodo.springforum.model.enity.User;

/**
 * Created by i.kotelnikov on 15.09.2017.
 */
public class OutputUser {

    private int id;
    private String username;

    public OutputUser(User user){
        this.setId(user.getId());
        this.setUsername(user.getUsername());
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
