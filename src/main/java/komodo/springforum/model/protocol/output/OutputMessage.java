package komodo.springforum.model.protocol.output;

import komodo.springforum.model.enity.Message;
import komodo.springforum.model.enity.User;
import komodo.springforum.model.protocol.websocket.WsMessageType;

import java.text.SimpleDateFormat;

/**
 * Created by i.kotelnikov on 15.09.2017.
 */
public class OutputMessage {

    private int id;
    private WsMessageType type = WsMessageType.USER_MESSAGE;
    private MessageUser author;
    private String content;
    private String timestamp;

    public OutputMessage(Message message){
        this.setId(message.getId());
        this.setAuthor(new MessageUser(message.getAuthor()));
        this.setContent(message.getContent());
//        this.setTimestamp(DateFormatter.formatDate(message.getTimestamp()));
        this.setTimestamp(new SimpleDateFormat("HH:mm:ss dd.MM.YYYY").format(message.getTimestamp()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setAuthor(MessageUser author) {
        this.author = author;
    }

    public MessageUser getAuthor() {
        return author;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public WsMessageType getType() {
        return type;
    }

    public void setType(WsMessageType type) {
        this.type = type;
    }

    class MessageUser {

        private int id;
        private String username;

        public MessageUser(User user){
            this.setId(user.getId());
            this.setUsername(user.getUsername());
        }

        public int getId() {
            return id;
        }

        public String getUsername() {
            return username;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
