package komodo.springforum.model.protocol.websocket;

/**
 * Created by i.kotelnikov on 21.09.2017.
 */
public enum WsMessageParameter {

    CHAT_ROOM_NAME,

    LAST_MESSAGE_ID,

    NUMBER_OF_MESSAGES

}
