package komodo.springforum.model.protocol.websocket;

/**
 * Created by i.kotelnikov on 21.09.2017.
 */
public enum WsMessageType {

    ENTER_CHAT_ROOM,

    EXIT_CHAT_ROOM,

    USER_MESSAGE,

    GET_CHAT_HISTORY,

    GET_CHAT_ROOM_DETAILS

}
