package komodo.springforum.model.protocol.websocket;

import java.util.Map;

/**
 * Created by i.kotelnikov on 21.09.2017.
 */
public class WsMessage {

    private long id;

    private WsMessageStatus status;

    private WsMessageType type;

    private String content;

    private Map<WsMessageParameter, String> params;


    public WsMessage(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WsMessageStatus getStatus() {
        return status;
    }

    public void setStatus(WsMessageStatus status) {
        this.status = status;
    }

    public Map<WsMessageParameter, String> getParams() {
        return params;
    }

    public void setParams(Map<WsMessageParameter, String> params) {
        this.params = params;
    }

    public WsMessageType getType() {
        return type;
    }

    public void setType(WsMessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getParameterValue(WsMessageParameter parameter) throws Exception{
        if (!params.containsKey(parameter)){
            throw new Exception("No such parameter!");
        }
        return params.get(parameter);
    }

}
