package komodo.springforum.model.protocol.websocket;

/**
 * Created by i.kotelnikov on 22.09.2017.
 */
public enum WsMessageStatus {

    SUCCESS,

    FAIL

}
