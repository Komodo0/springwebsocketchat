package komodo.springforum.model.form;

/**
 * Created by i.kotelnikov on 04.10.2017.
 */

public class RegistrationForm {

    private String username;

    private String password;

    private String confirmPassword;

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

}
