package komodo.springforum.service.entity;

import komodo.springforum.dao.enity.ChatRoomDao;
import komodo.springforum.model.enity.ChatRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by i.kotelnikov on 19.09.2017.
 */
@Service
public class ChatRoomServiceImpl implements ChatRoomService {

    @Autowired
    private ChatRoomDao chatRoomDao;

    @Transactional
    public void add(ChatRoom chatRoom) {
        chatRoomDao.add(chatRoom);
    }

    @Transactional
    public void edit(ChatRoom chatRoom) {
        chatRoomDao.edit(chatRoom);
    }

    @Transactional
    public void delete(int chatRoomId) {
        chatRoomDao.delete(chatRoomId);
    }

    @Transactional
    public ChatRoom getChatRoom(int chatRoomId) {
        return chatRoomDao.getChatRoom(chatRoomId);
    }

    @Transactional
    public ChatRoom getChatRoomByName(String chatRoomName) {
        return chatRoomDao.getChatRoomByName(chatRoomName);
    }

    @Transactional
    public ChatRoom getChatRoomByNameOrCreate(String chatRoomName) {
        if (chatRoomDao.getChatRoomByName(chatRoomName) == null){
            chatRoomDao.add(new ChatRoom(chatRoomName));
        }
        return chatRoomDao.getChatRoomByName(chatRoomName);
    }

    @Transactional
    public List getAllChatRooms() {
        return chatRoomDao.getAllChatRooms();
    }


}
