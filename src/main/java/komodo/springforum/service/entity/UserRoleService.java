package komodo.springforum.service.entity;

import komodo.springforum.model.enity.UserRole;

import java.util.List;

/**
 * Created by i.kotelnikov on 03.10.2017.
 */
public interface UserRoleService {

    public void add(UserRole userRole);

    public void edit(UserRole userRole);

    public void delete(int userRoleId);

    public UserRole getUserRole(int userRoleId);

    public UserRole getUserRoleByName(String roleName);

    public List getAllUserRoles();

}
