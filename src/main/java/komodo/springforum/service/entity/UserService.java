package komodo.springforum.service.entity;

import komodo.springforum.model.enity.User;

import java.util.List;

/**
 * Created by i.kotelnikov on 11.09.2017.
 */
public interface UserService {

    public void add(User user);

    public void edit(User user);

    public void delete(int userId);

    public User getUser(int userId);

    public User getUserByUsername(String username);

    public List getAllUsers();

    public boolean registerNewUser(String username, String password);

    public boolean userExists(String username);

}
