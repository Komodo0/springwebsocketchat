package komodo.springforum.service.entity;

import komodo.springforum.dao.enity.MessageDao;
import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by i.kotelnikov on 08.09.2017.
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Transactional
    public void add(Message message) {
        messageDao.add(message);
    }

    @Transactional
    public void edit(Message message) {
        messageDao.edit(message);
    }

    @Transactional
    public void delete(int messageId) {
        messageDao.delete(messageId);
    }

    @Transactional
    public Message getMessage(int messageId) {
        return messageDao.getMessage(messageId);
    }

    @Transactional
    public List getAllMessages() {
        return messageDao.getAllMessages();
    }

    @Transactional
    public List getMessages(int lastMessageId, int messageCount, ChatRoom chatRoom) {
        return messageDao.getMessages(lastMessageId, messageCount, chatRoom);
    }
}
