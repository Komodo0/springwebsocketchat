package komodo.springforum.service.entity;

import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.Message;

import java.util.List;

/**
 * Created by i.kotelnikov on 08.09.2017.
 */
public interface MessageService {

    public void add(Message message);

    public void edit(Message message);

    public void delete(int messageId);

    public Message getMessage(int messageId);

    public List getAllMessages();

    public List getMessages(int lastMessageId, int messageCount, ChatRoom chatRoom);

}
