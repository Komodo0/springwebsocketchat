package komodo.springforum.service.entity;

import komodo.springforum.dao.enity.UserDao;
import komodo.springforum.model.enity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by i.kotelnikov on 11.09.2017.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleService userRoleService;

    @Transactional
    public void add(User user) {
        userDao.add(user);
    }

    @Transactional
    public void edit(User user) {
        userDao.edit(user);
    }

    @Transactional
    public void delete(int userId) {
        userDao.delete(userId);
    }

    @Transactional
    public User getUser(int userId) {
        return userDao.getUser(userId);
    }

    @Transactional
    public User getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

    @Transactional
    public List getAllUsers() {
        return userDao.getAllUsers();
}

    @Transactional
    public boolean registerNewUser(String username, String password){
        User user = getUserByUsername(username);
        if (user == null){
            user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.addUserRole(userRoleService.getUserRoleByName("USER"));
            add(user);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean userExists(String username){
        return (getUserByUsername(username) != null);
    }
}
