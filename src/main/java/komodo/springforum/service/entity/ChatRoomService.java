package komodo.springforum.service.entity;

import komodo.springforum.model.enity.ChatRoom;

import java.util.List;

/**
 * Created by i.kotelnikov on 19.09.2017.
 */
public interface ChatRoomService {

    public void add(ChatRoom chatRoom);

    public void edit(ChatRoom chatRoom);

    public void delete(int chatRoomId);

    public ChatRoom getChatRoom(int chatRoomId);

    public ChatRoom getChatRoomByName(String chatRoomName);

    public ChatRoom getChatRoomByNameOrCreate(String chatRoomName);

    public List getAllChatRooms();


}
