package komodo.springforum.service.entity;

import komodo.springforum.dao.enity.UserRoleDao;
import komodo.springforum.model.enity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by i.kotelnikov on 03.10.2017.
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDao userRoleDao;

    @Transactional
    public void add(UserRole userRole) {
        userRoleDao.add(userRole);
    }

    @Transactional
    public void edit(UserRole userRole) {
        userRoleDao.edit(userRole);
    }

    @Transactional
    public void delete(int userRoleId) {
        userRoleDao.delete(userRoleId);
    }

    @Transactional
    public UserRole getUserRole(int userRoleId) {
        return userRoleDao.getUserRole(userRoleId);
    }

    @Transactional
    public UserRole getUserRoleByName(String roleName) {
        return userRoleDao.getUserRoleByName(roleName);
    }

    @Transactional
    public List getAllUserRoles() {
        return userRoleDao.getAllUserRoles();
    }


}
