package komodo.springforum.service.websocket;

import komodo.springforum.dao.websocket.OnlineChatRooms;
import komodo.springforum.dao.websocket.OnlineUsers;
import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.User;
import komodo.springforum.service.entity.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

/**
 * Created by i.kotelnikov on 02.10.2017.
 */
@Service
@Scope("singleton")
public class OnlineUsersService {

    @Autowired
    OnlineUsers onlineUsers;

    @Autowired
    OnlineChatRooms onlineChatRooms;

    @Autowired
    UserService userService;

    public void addOnlineUser(User user){
        onlineUsers.addUser(user);
    }

    public void removeOnlineUser(String username){
        onlineUsers.removeUser(username);
    }

    public User getOnlineUser(String username){
        return onlineUsers.getUser(username);
    }

    public void disconnectUser(String username){
        User user = getOnlineUser(username);
        if (user != null){
            try {
                user.getWebSocketSession().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            removeOnlineUser(username);
            user.setWebSocketSession(null);
            ChatRoom chatRoom = user.getChatRoom();
            if (chatRoom != null){
                chatRoom.removeUser(username);
                user.setChatRoom(null);
            }
        }
    }

    public void handleNewSession(WebSocketSession session){
        String username = session.getPrincipal().getName();
        User user = getOnlineUser(username);
        if (user == null){
            user = userService.getUserByUsername(username);
            user.setWebSocketSession(session);
            addOnlineUser(user);
        } else {
            try {
                user.getWebSocketSession().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (user.getChatRoom() != null){
                user.getChatRoom().removeUser(username);
                if (user.getChatRoom().isEmpty()) {
                    onlineChatRooms.removeChatRoom(user.getChatRoom().getName());
                }
                user.setChatRoom(null);
            }
            user.setWebSocketSession(session);
        }
    }

    public void handleSessionClosed(WebSocketSession session, CloseStatus status){
        String username = session.getPrincipal().getName();
        User user = getOnlineUser(username);
        if (user != null){
            removeOnlineUser(username);
            user.setWebSocketSession(null);
            ChatRoom chatRoom = user.getChatRoom();
            if (chatRoom != null){
                chatRoom.removeUser(username);
                user.setChatRoom(null);
                if (chatRoom.isEmpty()){
                    onlineChatRooms.removeChatRoom(chatRoom.getName());
                }
            }
        }
    }

    public void sendMessageToUser(User user, String jsonMessage){
        try {
            getOnlineUser(user.getUsername()).getWebSocketSession().sendMessage(new TextMessage(jsonMessage));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
