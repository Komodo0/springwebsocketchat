package komodo.springforum.service.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import komodo.springforum.model.protocol.websocket.WsMessage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;

import java.io.IOException;

/**
 * Created by i.kotelnikov on 22.09.2017.
 */
@Service
@Scope("singleton")
public class MessageParser {

    public WsMessage parseTextMessage(TextMessage textMessage) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        WsMessage wsMessage = mapper.readValue(textMessage.getPayload(), WsMessage.class);
        return wsMessage;
    }

}
