package komodo.springforum.service.websocket;

import komodo.springforum.dao.websocket.OnlineChatRooms;
import komodo.springforum.dao.websocket.OnlineUsers;
import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.User;
import komodo.springforum.service.entity.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;

import java.io.IOException;
import java.util.Set;

/**
 * Created by i.kotelnikov on 03.10.2017.
 */
@Service
public class OnlineChatRoomsService {

    @Autowired
    OnlineChatRooms onlineChatRooms;

    public void addOnlineChatRoom(ChatRoom chatRoom){
        onlineChatRooms.addChatRoom(chatRoom);
    }

    public void removeOnlineChatRoom(String chatRoomName){
        onlineChatRooms.removeChatRoom(chatRoomName);
    }

    public ChatRoom getOnlineChatRoom(String chatRoomName){
        return onlineChatRooms.getChatRoom(chatRoomName);
    }

    public void sendMessageToChatRoom(String chatRoomName, String message){
        ChatRoom chatRoom = getOnlineChatRoom(chatRoomName);
        if (chatRoom != null){
            Set<User> users = chatRoom.getUsers();
            for (User user : users){
                try {
                    user.getWebSocketSession().sendMessage(new TextMessage(message));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
