package komodo.springforum.service.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.Message;
import komodo.springforum.model.enity.User;
import komodo.springforum.model.protocol.output.OutputMessage;
import komodo.springforum.model.protocol.websocket.WsMessage;
import komodo.springforum.model.protocol.websocket.WsMessageParameter;
import komodo.springforum.service.entity.ChatRoomService;
import komodo.springforum.service.entity.MessageService;
import komodo.springforum.service.entity.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static komodo.springforum.model.protocol.websocket.WsMessageParameter.CHAT_ROOM_NAME;
import static komodo.springforum.model.protocol.websocket.WsMessageParameter.LAST_MESSAGE_ID;

/**
 * Created by i.kotelnikov on 21.09.2017.
 */
@Service
public class WebSocketMessageHandlerService {

    @Autowired
    UserService userService;

    @Autowired
    ChatRoomService chatRoomService;

    @Autowired
    MessageService messageService;

    @Autowired
    MessageParser messageParser;

    @Autowired
    OnlineUsersService onlineUsersService;

    @Autowired
    OnlineChatRoomsService onlineChatRoomsService;


    @Transactional(rollbackFor=Exception.class)
    public void handle(WebSocketSession session, TextMessage textMessage){
        try {
            String username = session.getPrincipal().getName();
            User user = onlineUsersService.getOnlineUser(username);
            if (user == null){
                user = userService.getUserByUsername(username);
                onlineUsersService.addOnlineUser(user);
            }

            WsMessage protocolMessage = messageParser.parseTextMessage(textMessage);

            switch (protocolMessage.getType()) {
                case USER_MESSAGE:
                    if (user.getChatRoom() != null && !protocolMessage.getContent().equals("")){
                        Message message = new Message();
                        message.setContent(protocolMessage.getContent());
                        message.setTimestamp(new Date());
                        message.setChatRoom(user.getChatRoom());
                        message.setAuthor(user);
                        messageService.add(message);
                        OutputMessage outputMessage = new OutputMessage(message);
                        String jsonMessage = new ObjectMapper().writeValueAsString(outputMessage);
                        onlineChatRoomsService.sendMessageToChatRoom(user.getChatRoom().getName(), jsonMessage);
                    }
                    break;

                case ENTER_CHAT_ROOM:
                    String chatRoomName = protocolMessage.getParameterValue(CHAT_ROOM_NAME).trim();
                    ChatRoom chatRoom1 = onlineChatRoomsService.getOnlineChatRoom(chatRoomName);
                    if (chatRoom1 == null){
                        chatRoom1 = chatRoomService.getChatRoomByNameOrCreate(chatRoomName);
                        onlineChatRoomsService.addOnlineChatRoom(chatRoom1);
                    }
                    user.setChatRoom(chatRoom1);
                    chatRoom1.addUser(user);
                    chatRoom1.addLoggedUserToHistory(user);
                    chatRoomService.edit(chatRoom1);
                    break;

                case EXIT_CHAT_ROOM:
                    ChatRoom chatRoom2 = user.getChatRoom();
                    chatRoom2.removeUser(username);
                    user.setChatRoom(null);
                    if (chatRoom2.isEmpty()){
                        onlineChatRoomsService.removeOnlineChatRoom(chatRoom2.getName());
                    }
                    break;

                case GET_CHAT_HISTORY:
                    if (user.getChatRoom() != null){
                        int lastMessageId = Integer.parseInt(protocolMessage.getParameterValue(WsMessageParameter.LAST_MESSAGE_ID));
                        int messagesCount = Integer.parseInt(protocolMessage.getParameterValue(WsMessageParameter.NUMBER_OF_MESSAGES));
                        List<Message> messages = messageService.getMessages(lastMessageId, messagesCount, user.getChatRoom());
                        for (Message message : messages){
                            String jsonMessage = new ObjectMapper().writeValueAsString(new OutputMessage(message));
                            onlineUsersService.sendMessageToUser(user, jsonMessage);
                        }
                    }
                    break;

                case GET_CHAT_ROOM_DETAILS:
                    break;
            }
        } catch (IOException e){
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
