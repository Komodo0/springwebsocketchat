package komodo.springforum.dao.enity;

import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.Message;

import java.util.List;

/**
 * Created by i.kotelnikov on 19.09.2017.
 */
public interface ChatRoomDao {

    public void add(ChatRoom chatRoom);

    public void edit(ChatRoom chatRoom);

    public void delete(int chatRoomId);

    public ChatRoom getChatRoom(int chatRoomId);

    public ChatRoom getChatRoomByName(String chatRoomName);

    public List getAllChatRooms();

}
