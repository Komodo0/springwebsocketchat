package komodo.springforum.dao.enity;

import komodo.springforum.model.enity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by i.kotelnikov on 11.09.2017.
 */
@Repository
public class UserDaoHibernate implements UserDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(User user) {
        session.getCurrentSession().save(user);
    }

    @Override
    public void edit(User user) {
        session.getCurrentSession().update(user);
    }

    @Override
    public void delete(int userId) {
        session.getCurrentSession().delete(getUser(userId));
    }

    @Override
    public User getUser(int userId) {
        return (User) session.getCurrentSession().get(User.class, userId);
    }

    @Override
    public User getUserByUsername(String username) {
        try {
            return (User) session.getCurrentSession().createQuery("select u from User u where u.username = :username").setParameter("username", username).setCacheable(false).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List getAllUsers() {
        return session.getCurrentSession().createQuery("from User").list();
    }

}
