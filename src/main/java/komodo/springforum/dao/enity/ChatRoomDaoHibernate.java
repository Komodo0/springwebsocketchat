package komodo.springforum.dao.enity;

import komodo.springforum.model.enity.ChatRoom;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by i.kotelnikov on 19.09.2017.
 */
@Repository
public class ChatRoomDaoHibernate implements ChatRoomDao{

    @Autowired
    private SessionFactory session;

    @Override
    public void add(ChatRoom chatRoom) {
        session.getCurrentSession().save(chatRoom);
    }

    @Override
    public void edit(ChatRoom chatRoom) {
        session.getCurrentSession().update(chatRoom);
    }

    @Override
    public void delete(int chatRoomId) {
        session.getCurrentSession().delete(getChatRoom(chatRoomId));
    }

    @Override
    public ChatRoom getChatRoom(int chatRoomId) {
        return (ChatRoom) session.getCurrentSession().get(ChatRoom.class, chatRoomId);
    }

    @Override
    public ChatRoom getChatRoomByName(String chatRoomName) {
        try {
            return (ChatRoom) session.getCurrentSession()
                    .createQuery("select c from ChatRoom c where c.name = :chatRoomName")
                    .setParameter("chatRoomName", chatRoomName)
                    .setCacheable(true)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List getAllChatRooms() {
        return session.getCurrentSession().createQuery("from ChatRoom").list();
    }

}
