package komodo.springforum.dao.enity;

import komodo.springforum.model.enity.User;
import komodo.springforum.model.enity.UserRole;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by i.kotelnikov on 03.10.2017.
 */
@Repository
public class UserRoleDaoHibernate implements UserRoleDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(UserRole userRole) {
        session.getCurrentSession().save(userRole);
    }

    @Override
    public void edit(UserRole userRole) {
        session.getCurrentSession().update(userRole);
    }

    @Override
    public void delete(int userRoleId) {
        session.getCurrentSession().delete(getUserRole(userRoleId));
    }

    @Override
    public UserRole getUserRole(int userRoleId) {
        return (UserRole) session.getCurrentSession().get(UserRole.class, userRoleId);
    }

    @Override
    public UserRole getUserRoleByName(String userRoleName) {
        try {
            return (UserRole) session.getCurrentSession().createQuery("select u from UserRole u where u.role = :userRoleName").setParameter("userRoleName", userRoleName).setCacheable(true).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List getAllUserRoles() {
        return session.getCurrentSession().createQuery("from UserRole").list();
    }
}
