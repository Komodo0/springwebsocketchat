package komodo.springforum.dao.enity;

import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.Message;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by i.kotelnikov on 08.09.2017.
 */
@Repository
public class MessageDaoHibernate implements MessageDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(Message message) {
        session.getCurrentSession().save(message);
    }

    @Override
    public void edit(Message message) {
        session.getCurrentSession().update(message);
    }

    @Override
    public void delete(int messageId) {
        session.getCurrentSession().delete(getMessage(messageId));
    }

    @Override
    public Message getMessage(int messageId) {
        return (Message) session.getCurrentSession().get(Message.class, messageId);
    }

    @Override
    public List getAllMessages() {
        return session.getCurrentSession().createQuery("from Message").list();
    }

    @Override
    public List getMessages(int lastMessageId, int messageCount, ChatRoom chatRoom) {
        Query query;
        if (lastMessageId == -1){
            query = session.getCurrentSession().createQuery("FROM Message m WHERE m.chatRoom = :chatRoom ORDER BY m.id ASC");
        } else {
            query = session.getCurrentSession().createQuery("FROM Message m WHERE m.id < :lastId AND m.chatRoom = :chatRoom ORDER BY m.id ASC");
            query.setParameter("lastId", lastMessageId);
        }
        query.setParameter("chatRoom", chatRoom);
//        query.setMaxResults(messageCount);
        return query.list();
    }


}
