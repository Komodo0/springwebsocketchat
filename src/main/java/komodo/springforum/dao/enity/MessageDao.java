package komodo.springforum.dao.enity;

import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.Message;

import java.util.List;

/**
 * Created by i.kotelnikov on 04.09.2017.
 */
public interface MessageDao {

   public void add(Message message);

   public void edit(Message message);

   public void delete(int messageId);

   public Message getMessage(int messageId);

   public List getAllMessages();

   public List getMessages(int lastMessageId, int messageCount, ChatRoom chatRoom);

}
