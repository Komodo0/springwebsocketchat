package komodo.springforum.dao.websocket;

import komodo.springforum.model.enity.ChatRoom;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by i.kotelnikov on 02.10.2017.
 */
@Repository
@Scope("singleton")
public class OnlineChatRooms {

    volatile Set<ChatRoom> chatRooms = new HashSet<>();

    @Transactional
    public synchronized void addChatRoom(ChatRoom chatRoom){
        chatRooms.add(chatRoom);
    }

    @Transactional
    public synchronized void removeChatRoom(String chatRoomName){
        for (ChatRoom chatRoom : chatRooms) {
            if (chatRoom.getName().equals(chatRoomName)) {
                chatRooms.remove(chatRoom);
                break;
            }
        }
    }

    @Transactional
    public synchronized ChatRoom getChatRoom(String chatRoomName){
        for (ChatRoom chatRoom : chatRooms) {
            if (chatRoom.getName().equals(chatRoomName)) {
                return chatRoom;
            }
        }
        return null;
    }

}
