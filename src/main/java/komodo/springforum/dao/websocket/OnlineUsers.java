package komodo.springforum.dao.websocket;

import komodo.springforum.model.enity.ChatRoom;
import komodo.springforum.model.enity.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by i.kotelnikov on 02.10.2017.
 */
@Repository
@Scope("singleton")
public class OnlineUsers {

    volatile Set<User> users = new HashSet<>();

    @Transactional
    public synchronized void addUser(User user){
        this.users.add(user);
    }

    @Transactional
    public synchronized void removeUser(String username){
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                users.remove(user);
                break;
            }
        }
    }

    @Transactional
    public synchronized User getUser(String username){
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

}
