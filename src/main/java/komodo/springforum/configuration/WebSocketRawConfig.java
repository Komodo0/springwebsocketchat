package komodo.springforum.configuration;

import komodo.springforum.controller.ChatWsHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Created by i.kotelnikov on 21.09.2017.
 */
@Configuration
@EnableWebSocket
public class WebSocketRawConfig implements WebSocketConfigurer {

    @Autowired
    ChatWsHandler chatWsHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(chatWsHandler, "/chat").withSockJS();
    }


}
