package komodo.springforum.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableLoadTimeWeaving;

/**
 * Created by i.kotelnikov on 01.09.2017.
 */
@Configuration
@ComponentScan(basePackages = "komodo.springforum")
public class AppConfig {

}
