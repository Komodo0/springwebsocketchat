package komodo.springforum.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by i.kotelnikov on 11.09.2017.
 */
public class SecurityWebAppInitializer extends AbstractSecurityWebApplicationInitializer {
}
