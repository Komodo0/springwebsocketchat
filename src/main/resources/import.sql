INSERT INTO user_role (`role_id`, `role`) VALUES (1, 'ADMIN');
INSERT INTO user_role (`role_id`, `role`) VALUES (2, 'USER');

INSERT INTO user (`user_id`, `username`, `password`) VALUES (1, 'admin', 'password');
INSERT INTO user_to_user_role (`role_id`, `user_id`) VALUES (1, 1);

INSERT INTO user (`user_id`, `username`, `password`) VALUES (2, 'user', 'password');
INSERT INTO user_to_user_role (`role_id`, `user_id`) VALUES (2, 2);