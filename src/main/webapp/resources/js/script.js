/**
 * Created by i.kotelnikov on 01.09.2017.
 */

$(window).ready(function() {
    var chatRoomName = null;
    var socket = null;
    inChatRoom(false);
    $('#content').keypress(function(event){
        if(event.keyCode == 13){
            $('#sendName').click();
        }
    });
    $('#roomName').keypress(function(event){
        if(event.keyCode == 13){
            $('#enterChatRoom').click();
        }
    });
    connect();
});

function inChatRoom(inChatRoom) {
    $("#roomName").prop('disabled', inChatRoom);
    inChatRoom ? $("#exitChatRoom").show() : $("#exitChatRoom").hide();
    inChatRoom ? $("#enterChatRoom").hide() : $("#enterChatRoom").show();
    inChatRoom ? $("#chatRoomForm").show() : $("#chatRoomForm").hide();
    if (!inChatRoom) {$('#response').html('');}

}

function connect() {
    socket = new SockJS(contextRoot + '/chat');
    setTimeout(function () {
        if (socket.readyState == 1){
            console.log("Connected! Ready state = " + socket.readyState);
            socket.onmessage = function(message){
                handleWebSocketMessage(message);
            };
        } else {
            console.log("Not Connected! Ready state = " + socket.readyState);
            setTimeout(connect(), 15000);
        }
    }, 1000);
}

function enterChatRoom() {
    chatRoomName = document.getElementById('roomName').value;
    enterChatRoomByName(chatRoomName);
    askGetMessages(-1);
    inChatRoom(true);
}

function askGetMessages(last_message_id) {
    var message = new Object();
    message.id = Date.now();
    message.type = "GET_CHAT_HISTORY";
    message.params = new Object();
    message.params.LAST_MESSAGE_ID = last_message_id;
    message.params.NUMBER_OF_MESSAGES = 10;
    socket.send(JSON.stringify(message));
    console.log("Send: " + JSON.stringify(message));
}

function enterChatRoomByName(roomName) {
    var message = new Object();
    message.id = Date.now();
    message.type = "ENTER_CHAT_ROOM";
    message.params = new Object();
    message.params.CHAT_ROOM_NAME = roomName;
    socket.send(JSON.stringify(message));
    console.log("Send: " + JSON.stringify(message));
    console.log("Enter chat room: " + roomName);
}

function exitChatRoom() {
    inChatRoom(false);
    var message = new Object();
    message.id = Date.now();
    message.type = "EXIT_CHAT_ROOM";
    socket.send(JSON.stringify(message));
    console.log("Send: " + JSON.stringify(message));
    console.log("Exit chat room");
}

function sendMessage() {
    var message = new Object();
    message.id = Date.now();
    message.type = "USER_MESSAGE";
    message.content = document.getElementById('content').value;
    document.getElementById('content').value = '';
    socket.send(JSON.stringify(message));
    console.log("Send: " + JSON.stringify(message));
}

function handleWebSocketMessage(message) {
    console.log("Recieve:" + message.data);

    json_message = JSON.parse(message.data);
    switch (json_message.type){
        case "USER_MESSAGE":
            showMessage(message);
            break;
    }

}

function showMessage(message) {
    console.log("Recieve: " + message.data);
    responseDiv = $('#response');
    responseDiv.on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function(){
        responseDiv.stop();
    });
    json_message = JSON.parse(message.data);
    responseDiv.append(createMessage(json_message));
    destination = responseDiv[0].scrollHeight;
    responseDiv.animate({ scrollTop: destination }, 0);
}

function createMessage(json_data){
     message =
         '<div class="list-group-item message message_' + json_data.id +'">' +
            '<div>' +
                '<p class="list-group-item-heading">' +
                    '<a href="#" class="author author_' + json_data.author.id +'">' +
                        json_data.author.username +
                    '</a>' +
                    '<small class="float-right">' +
                        json_data.timestamp +
                    '</small>' +
                '</p>' +
                '<div>' +
                    '<div>' +
                        json_data.content +
                    '</div>' +
                '</div>' +
             '</div>' +
         '</div>';

     return message;
}