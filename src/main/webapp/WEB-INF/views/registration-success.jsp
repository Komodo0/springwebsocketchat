
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
    <jsp:include page="components/head.jsp" />
</head>
<body>
<jsp:include page="components/header.jsp" />
<div class="container">
    <br/>
    <h1>Successfull Registration</h1>
    <hr/><br/>
    <div class="row">
        <div class="col-sm-12">
            <p>Congratulations! Your registration was successful</p>
            <p>Now you can <a href="${pageContext.request.contextPath}/" title="Login">login</a>
                or
                <a href="${pageContext.request.contextPath}/init" title="Register user">register one more user</a>!</p>
        </div>
    </div>
</div>
</body>
</html>