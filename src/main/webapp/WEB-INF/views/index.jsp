<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: i.kotelnikov
  Date: 01.09.2017
  Time: 12:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <jsp:include page="components/head.jsp" />
        <script type="text/javascript">var contextRoot = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '') + "${pageContext.request.contextPath}"</script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
    </head>
  <body>
  <jsp:include page="components/header.jsp" />
  <div class="container">
      <br/>
      <h1>Welcome to WebSocket chat!</h1>
      <hr/><br/>
      <div class="row">
          <div class="col-sm-12">

              <div id="enterChatRoomForm" class="form-group row">
                  <div class="col-sm-1"></div>
                  <div class="col-sm-10">
                      <div class="input-group mb-2 mb-sm-0">
                          <input type="text" class="form-control" id="roomName" placeholder="Chat room name"/>
                          <button id="enterChatRoom" class="btn btn-primary" onclick="enterChatRoom();" style="margin-left:20px;">Enter ChatRoom</button>
                          <button id="exitChatRoom" class="btn btn-primary" onclick="exitChatRoom();" style="margin-left:20px;">Exit ChatRoom</button>
                      </div>
                  </div>
                  <div class="col-sm-1"></div>
              </div>


              <div id="chatRoomForm" class="form-groupp row">
                  <div class="col-sm-1"></div>
                  <div class="col-sm-10">
                      <div class="input-group mb-2 mb-sm-0">
                          <div id="response" class="card" style="width:100%; height:500px; overflow-y: scroll;"></div>
                      </div>
                      <br>
                      <div class="input-group mb-2 mb-sm-0">
                          <input type="text" class="form-control" id="content" placeholder="Your message..." />
                          <button id="sendName" class="btn btn-primary" onclick="sendMessage();" style="margin-left:20px;">Send</button>
                      </div>
                  </div>
                  <div class="col-sm-1"></div>
              </div>

          </div>
      </div>
  </div>

  </body>
</html>
