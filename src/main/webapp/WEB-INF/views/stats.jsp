<%--
  Created by IntelliJ IDEA.
  User: i.kotelnikov
  Date: 05.10.2017
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="components/head.jsp" />
</head>
<body>
<jsp:include page="components/header.jsp" />


<div class="container">
    <br/>
    <h1>Statistic</h1>
    <hr/><br/>
    <div class="row">
        <div class="col-sm-12">
            <c:choose>
                <c:when test="${ chatRooms.size() eq 0 ? true : false}">
                    No statistic yet.
                </c:when>
                <c:otherwise>
                    <table id="statistic" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Room Name</th>
                                <th>Messages Count</th>
                                <th>Users Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${chatRooms}" var="chatRoom">
                                <tr>
                                    <td>${chatRoom.name}</td>
                                    <td>${chatRoom.messages.size()}</td>
                                    <td>${chatRoom.loggedUsersHistory.size()}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <script>
                        $(document).ready(function () {
                            $("#statistic").DataTable({
                                "lengthChange": false,
                                "pageLength": 15,
                                "pagingType": "numbers"
                            });
                        });
                    </script>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>



</body>
</html>
