<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
    <jsp:include page="components/head.jsp" />
    <style type="text/css">
        span.error {
            color: red;
        }
    </style>
</head>
<body>
<jsp:include page="components/header.jsp" />






<div class="container">
    <br/>
    <h1>Registration</h1>
    <hr/><br/>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <form:form method='POST' modelAttribute="registrationForm">

                <c:set var="usernameErrors"><form:errors path="username" /></c:set>
                <c:set var="passwordErrors"><form:errors path="password" /></c:set>
                <c:set var="confirmPasswordErrors"><form:errors path="confirmPassword" /></c:set>

                <c:if test="${not empty usernameErrors}">
                    <div class="alert alert-danger" role="alert">
                        ${usernameErrors}
                    </div>
                </c:if>
                <c:if test="${not empty passwordErrors}">
                    <div class="alert alert-danger" role="alert">
                            ${passwordErrors}
                    </div>
                </c:if>
                <c:if test="${not empty confirmPasswordErrors}">
                    <div class="alert alert-danger" role="alert">
                            ${confirmPasswordErrors}
                    </div>
                </c:if>


                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="sr-only" for="inlineFormInputGroupUsername">Username</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">@</div>
                            <input type='text' class="form-control" id="inlineFormInputGroupUsername" placeholder="Username" name='username'>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="sr-only" for="inlineFormInputGroupPassword">Password</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">#</div>
                            <input type='password' class="form-control" id="inlineFormInputGroupPassword" placeholder="Password" name='password'/>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label class="sr-only" for="inlineFormInputGroupConfirmPassword">Password</label>
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon">#</div>
                            <input type='password' class="form-control" id="inlineFormInputGroupConfirmPassword" placeholder="Confirm password" name='confirmPassword'/>
                        </div>
                    </div>
                </div>
                <div class="form-group row text-right">
                    <div class="col-sm-12">
                        <input name="submit" type="submit" class="btn btn-primary" value="Registration" />
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form:form>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>
</body>
</html>