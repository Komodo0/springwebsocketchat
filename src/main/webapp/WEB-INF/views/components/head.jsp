<%--
  Created by IntelliJ IDEA.
  User: i.kotelnikov
  Date: 05.10.2017
  Time: 10:43
  To change this template use File | Settings | File Templates.
--%>
    <title>WebSocketChat</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/style.css" type="text/css"/>
    <link rel='stylesheet' href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" type="text/css"/>
    <link rel='stylesheet' href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" type="text/css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" type="application/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="application/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="application/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="application/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/0.3.4/sockjs.min.js" type="application/javascript"></script>